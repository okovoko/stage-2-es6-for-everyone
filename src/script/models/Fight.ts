import { Fighter } from './fighter';

export class Fight{
    private _leftFighter: Fighter;
    private _rightFighter: Fighter;
    private _fightResults: Array<[number, number]>;

    public async startFight(leftFighter: Fighter, rightFighter: Fighter)
    {
        this._leftFighter = leftFighter;
        this._rightFighter = rightFighter;
        this._fightResults.push([this._leftFighter.health, this._rightFighter.health]);

        const leftToRightDamage = this._leftFighter.getHitPower() - this._rightFighter.getBlockPower();
        const rightToLeftDamage = this._rightFighter.getHitPower() - this._leftFighter.getBlockPower();

        do {
            this._leftFighter.health -= rightToLeftDamage * Math.round(Math.random());
            this._rightFighter.health -= leftToRightDamage * Math.round(Math.random());
            this._fightResults.push([this._leftFighter.health, this._rightFighter.health]);
        }
        while(this._leftFighter.health > 0 && this._rightFighter.health > 0)

        return this._fightResults;
    }
}