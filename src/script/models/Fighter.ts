export interface IFighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source:string;

    getHitPower(): number;
    getBlockPower(): number;
}

export class Fighter implements IFighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source:string;

    constructor(_id:number, name:string, attack:number, defense:number, health:number, source:string) {
        this._id = _id;
        this.name = name;
        this.attack = attack;
        this.defense = defense;
        this.health = health;
        this.source = source;
    }

    public getHitPower(): number {
        let criticalHitChance = Math.floor(1 + Math.random() * 2);
        let power = this.attack * criticalHitChance;
        return power;
    }
    
    public getBlockPower(): number {
        let dodgeChance = Math.floor(1 + Math.random() * 2);
        let power = this.defense * dodgeChance;
        return power;
    }
}