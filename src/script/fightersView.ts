import {View, Atribute} from './view';
import FighterView from './fighterView';
import FighterInfoView from "./fighterInfoView";
import { Fighter } from './models/Fighter';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  handleClick: EventHandlerNonNull;

  constructor(fighters: Fighter[]) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  private static _modal: HTMLDivElement = document.getElementById("fighter-info-modal") as HTMLDivElement;

  createFighters(fighters: Fighter[]) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }
  createFighterSelect(fighters: Fighter[], selectId:string) : HTMLSelectElement
    {
        const fighterSelectElement:HTMLSelectElement = document.createElement("select");

        fighterSelectElement.id = selectId;
        for (let fighter of fighters)
        {
          let option: HTMLElement = this.createElement(
            {
                tagName: "option",
                className: 'option',
                attributes: [
                  new Atribute("value", fighter._id.toString())
                ]
            });
          option.innerHTML = fighter.name;
          fighterSelectElement.appendChild(option);
        }
                
        fighterSelectElement.firstElementChild.setAttribute("selected", "true");

        return fighterSelectElement;
    }

  public createFightersSelectorMenu(fighters: Fighter[], startFightClickHandler: () => void): HTMLElement {
    const container = this.createElement({ tagName:"div", className:"menu" });
        const leftFighterSelect: HTMLSelectElement = this.createFighterSelect(fighters, "left-fighter");
        
        const vsElement: HTMLElement = this.createElement({
          tagName: "h3",
          className: "vs-element"
        });
        vsElement.innerHTML = "VS";
        const rightFighterSelect: HTMLSelectElement = this.createFighterSelect(fighters, "right-fighter");

        const startButton: HTMLButtonElement = this.createElement({
          tagName: "button",
          className: "start-button"
        }) as HTMLButtonElement;
        startButton.id = "start-button";
        startButton.innerHTML = "Start fight!";
        startButton.onclick = startFightClickHandler;
        container.append(leftFighterSelect, vsElement, rightFighterSelect, startButton);
        return container;
  }

  async handleFighterClick(event: Event, fighter: { _id: number; }) {
    if(!this.fightersDetailsMap.has(fighter._id))
    {
      fighter = await fighterService.getFighterDetails(fighter._id); 
      this.fightersDetailsMap.set(fighter._id, fighter);
    }
    
    try {
      const selectedFighter: Fighter = this.fightersDetailsMap.get(fighter._id);
      const fighterInfoView: View = new FighterInfoView(
        selectedFighter,
        this.fightersDetailsMap
      );
      FightersView._modal.append(fighterInfoView.element as Node);
      FightersView._modal.style.display = "block";
    } catch (error) {
      throw error;
    }
  }
}

export default FightersView;