class Atribute {
    key: string;
    value: string;

    public constructor(key: string, value: string) {
        this.key = key;
        this.value = value;
    }
}

class View {
    element: HTMLElement;
  
    createElement({ tagName='', className = '', attributes = [] }: { tagName : string, className?: string, attributes? : Atribute[]  }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      attributes.forEach(atr => element.setAttribute(atr.key, atr.value));
  
      return element;
    }
  }
  
  export {View, Atribute};