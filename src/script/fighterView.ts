import { View, Atribute } from './view';
import { Fighter } from './models/Fighter';

class FighterView extends View {
  constructor(fighter: Fighter, handleClick:Function) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter: Fighter, handleClick:Function): void {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter));
  }

  createName(name: string): HTMLElement {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source: string): HTMLElement {
    let attributes: Atribute[] = [
        new Atribute("src", source)
      ];
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;