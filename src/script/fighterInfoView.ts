import { View, Atribute } from './view';
import { Fighter } from './models/Fighter';

class FighterInfoView extends View {
  element: HTMLDivElement | null = null;
  private _fighter: Fighter;
  private _fightersMap: Map<number, Fighter>;

  constructor(fighter: Fighter, fightersMap: Map<number, Fighter>) {
    super();

    this._fighter = fighter;
    this._fightersMap = fightersMap;
    this._createFighterInfoView(fighter);
  }

  private static _modal: HTMLDivElement = document.getElementById("fighter-info-modal") as HTMLDivElement;

  private _createFighterInfoView(fighter: Fighter): void {
    this.element = this.createElement({
      tagName: "div",
      className: "fighter-info-view"
    }) as HTMLDivElement;

    const imageContainer: HTMLDivElement = this._createImageContainer(fighter.source as string) as HTMLDivElement;
    const infoContainer = this._createInfoContainer(fighter);
    const updateContainer = this._createUpdateContainer();
    const closeBtn = this._createCloseButton();
    infoContainer.append(updateContainer);

    this.element.append(imageContainer, infoContainer, closeBtn);
  }

  private _closeModal(): void {
    FighterInfoView._modal.innerHTML = "";
    FighterInfoView._modal.style.display = "none";
  }

  private _updateFighterClickHandler(): void {
    const thisElement = this.element as HTMLDivElement;
    const health: number = parseInt((thisElement.querySelector(".health-input") as HTMLInputElement).value);
    const attack: number = parseInt((thisElement.querySelector(".attack-input") as HTMLInputElement).value);
    const defense = parseInt((thisElement.querySelector(".defense-input") as HTMLInputElement).value);

    this._fighter.health = health;
    this._fighter.attack = attack;
    this._fighter.defense = defense;

    this._fightersMap.set(this._fighter._id, this._fighter);
    this._closeModal();
  }

  private _createImageContainer(source: string): HTMLDivElement {
    const imageContainer: HTMLDivElement = this.createElement({
      tagName: "div",
      className: "fighter-info-image-container"
    }) as HTMLDivElement;

    const image: HTMLImageElement = this.createElement({
      tagName: "img",
      className: "fighter-info-image",
      attributes: [
        new Atribute("src", source)
      ]
    }) as HTMLImageElement;
    imageContainer.append(image);

    return imageContainer;
  }

  private _createInputContainer(name: string, value: number | string): HTMLDivElement {
    const inputContainer: HTMLDivElement = this.createElement({
      tagName: "div",
      className: "fighter-input-container"
    }) as HTMLDivElement;

    const inputLabel: HTMLLabelElement = this.createElement({
      tagName: "label",
      className: "fighter-input-label"
    }) as HTMLLabelElement;
    inputLabel.innerHTML = name;

    const input: HTMLInputElement = this.createElement({
      tagName: "input",
      className: `${name.toLowerCase()}-input`,
      attributes: [
        new Atribute('type', 'number'),
        new Atribute('min', '0'),
        new Atribute('step', '1'),
        new Atribute('name', name)
      ]
    }) as HTMLInputElement;
    input.value = value.toString();
    inputContainer.append(inputLabel, input);
    return inputContainer;
  }

  private _createInfoContainer(fighter: Fighter): HTMLDivElement {
    const infoContainer: HTMLDivElement = this.createElement({
      tagName: "div",
      className: "fighter-info-container"
    }) as HTMLDivElement;

    const name: HTMLDivElement = this.createElement({
      tagName: "div",
      className: "fighter-name"
    }) as HTMLDivElement;
    name.innerHTML = fighter.name;

    const health: HTMLDivElement = this._createInputContainer("Health", fighter.health);
    const attack: HTMLDivElement = this._createInputContainer("Attack", fighter.attack);
    const defense: HTMLDivElement = this._createInputContainer("Defense", fighter.defense);

    infoContainer.append(name, health, attack, defense);
    return infoContainer;
  }

  private _createUpdateContainer(): HTMLDivElement {
    const updateContainer: HTMLDivElement = this.createElement({
      tagName: "div",
      className: "fighter-update-container"
    }) as HTMLDivElement;

    const updateButton: HTMLButtonElement = this.createElement({
      tagName: "button",
      className: "fighter-update-button"
    }) as HTMLButtonElement;
    updateButton.innerHTML = "Update";
    updateButton.onclick = this._updateFighterClickHandler.bind(this);
    updateContainer.append(updateButton);

    return updateContainer;
  }

  private _createCloseButton(): HTMLSpanElement {
    const closeButton: HTMLSpanElement = this.createElement({
      tagName: "span",
      className: "modal-close-button"
    }) as HTMLSpanElement;

    closeButton.innerHTML = "&#10060; Close";
    closeButton.addEventListener("click", this._closeModal, false);
    return closeButton;
  }
}

export default FighterInfoView;