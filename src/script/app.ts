import FightersView from './fightersView';
import FightView from './fightView';
import { fighterService } from './services/fightersService';
import { Fight } from './models/Fight';

class App {
  private _startFightClickHandler: () => void;
  private _fight: Fight;
  private _fightResult: Array<[number, number]>;

  constructor() {
    this._startFightClickHandler = this.startFightClickHandler.bind(this);
    this.startApp();
  }

  static rootElement = document.getElementById('root') as HTMLElement;
  static loadingElement = document.getElementById('loading-overlay') as HTMLElement;

  private async startFightClickHandler()
    {
        const leftSelect = document.getElementById("left-fighter") as HTMLSelectElement;
        const leftFighterId = leftSelect.options[leftSelect.selectedIndex].value;

        const rightSelect = document.getElementById("right-fighter") as HTMLSelectElement;
        const rightFighterId = leftSelect.options[rightSelect.selectedIndex].value;

        const leftFighter = await fighterService.getFighterDetails(+leftFighterId);
        const rightFighter = await fighterService.getFighterDetails(+rightFighterId);
                        
        this._fight = new Fight();
        this._fightResult = Object.assign([], this._fight.startFight(leftFighter, rightFighter));
        const fightView: FightView = new FightView();

        App.rootElement.innerHTML ='';
        App.rootElement.append(fightView.createFightView(leftFighter, rightFighter));
    }

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const menuElement = fightersView.createFightersSelectorMenu(fighters, this._startFightClickHandler);
      const fightersElement = fightersView.element;

      const fightView = new FightView();
       
      App.rootElement.appendChild(fightersElement);
      App.rootElement.appendChild(menuElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
        App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;