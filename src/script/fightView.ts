import { View, Atribute } from './view';
import { Fighter } from './models/Fighter';

class FightView extends View {

    private _backClickHandler(): void {
        this.element.innerHTML = "";
        this.element.style.display = "none";
      }

    createFightView(leftFighter: Fighter, rightFighter: Fighter) {
        const divFightersContainer:HTMLElement = this.createElement({ tagName: 'div', className: 'fight' });

        const leftFighterElement: HTMLElement = this.createFighter(leftFighter);
        const rightFighterElement: HTMLElement = this.createFighter(rightFighter, true);

        const fightResultElement = this.createElement(
            {
                tagName: "h3",  
                className: "fight-result" 
            });
        
        const backButton: HTMLButtonElement = this.createElement({
            tagName: "button",
            className: "fight-back-button"
          }) as HTMLButtonElement;
        backButton.innerHTML = "Back";
        backButton.disabled = true;
        backButton.onclick = this._backClickHandler.bind(this);

        divFightersContainer.append(fightResultElement, leftFighterElement, rightFighterElement, backButton);

        return divFightersContainer;
    }

    createFighter(fighter: Fighter, isRotated:boolean = false) : HTMLElement
    {
        const nameElement = this.createElement({tagName: "span",  className: "fighter-name"});
        nameElement.innerHTML = fighter.name;
        const atackElement = this.createElement({tagName: "span", className: "fighter-atack"});
        atackElement.innerHTML = "Attack: " + fighter.attack;
        const defenseElement = this.createElement({tagName: "span", className: "fighter-defense"});
        defenseElement.innerHTML = "Defense: " + fighter.defense;

        const imageElement: HTMLImageElement = this.createElement({
            tagName: "img",
            className: "fighter-image",
            attributes: [
              new Atribute("src", fighter.source)
            ]
          }) as HTMLImageElement;
        if (isRotated)
        {
            imageElement.style.transform = "scaleX(-1)";
        }

        const healthElement = this.createHealthBar(fighter.health);

        const divFighterElement = this.createElement({ tagName: 'div', className: 'fighter' });
        divFighterElement.append(nameElement, healthElement, imageElement, atackElement, defenseElement);

        return divFighterElement;
    }
    
    createHealthBar(healthLeft: number): HTMLElement
    {
        const divProgressElement: HTMLElement = this.createElement({ tagName: "div", className: "progress" });

        const divBarElement: HTMLElement = this.createElement({ tagName: "div", className: "bar"});
        divBarElement.style.width = healthLeft + '%';

        divProgressElement.appendChild(divBarElement);
        return divProgressElement;
    }
}

export default FightView;